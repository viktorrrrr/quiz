const array = [["Koliko je 2+2", "4"], ["Koji je glavni grad Srbije", "beograd"], ["Koji je najveci grad na svetu", "tokyo"],
["Najbrza zivotinja na svetu", "gepard"], ["Kako se zove predsednik Rusije", "vladimir putin"], ["Najveca zgrada na svetu je", "burj khalifa"],
["Najjaca graficka na svetu je", "big navi"], ["Najbogatija drzava na svetu", "usd"], ["Koliko miliona stanovnika ima Srbija", "7"],
["Najvisi covek na svetu je", "viktor"]
]
//brojac greski
let count = 0;
//brojac tacnih resenja
let countTrue = 0;
//array
const brojevi = [];

//generise random broj i vraca ga
function getRandomNumber() {
    var randomNumber = Math.floor(Math.random() * 10);
    if (!brojevi.includes(randomNumber)) {
        brojevi.push(randomNumber);
        console.log(brojevi);
        return randomNumber;
    }
    else {
        return getRandomNumber();
    }

}
//smesta random broj iz funkcije 
var broj = getRandomNumber();


//uzima input od korisnika
function getInput() {
    var input = document.querySelector(".odgovor");
    console.log(input.value);
    return input.value.toLowerCase();

}


//poziva dve funkcije kako bi napravio random pitanje i smestio ga u div
function randomQuestion() {
    return createDiv(broj);
}


//pravi sledece pitanje ako damo tacan odgovor
function nextQuestion() {


    if (count === 10) {
        let scorePercent = Math.round(countTrue / count * 100);
        return document.querySelector(".proba").innerHTML = `<p class="endValue">Uradili ste tacno: ${scorePercent}%</p><br><button type="button" onclick="(location.reload())">Zapocni quiz</button>`;
    }
    if (getInput() === array[broj][1]) {
        count += 1;
        countTrue += 1;
        document.querySelector(".posto").innerHTML = `<p class="counter"> ${count}/10</p><br>`
        broj = getRandomNumber();
        randomQuestion();
    }


    else {
        count += 1;
        document.querySelector(".posto").innerHTML = `<p class="counter"> ${count}/10</p><br>`
    }

}


//pravi Div sa svim elementima
function createDiv(number) {
    document.querySelector(".proba").innerHTML = `<div class="newDiv">
    <p>${array[number][0]}</p>
    <input type="text" class="odgovor" placeholder="Unesi odgovor"><br>
    <button type="button" onclick="(location.reload())">Prekini quiz</button>
    <button type="button" onclick="(nextQuestion())">Dalje</button>
    
     </div>`
}



//brise pocetno dugme "pocni quiz"
function removeButton() {
    document.querySelector(".quiz").remove();
}


